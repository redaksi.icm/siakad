<?php
$cek = mysqli_query($connect, "SELECT * from siswa_baru where id_aktivasi ='$_SESSION[id_user]'");
?>
<?php
if (mysqli_num_rows($cek) == 0) { ?>
    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
                <h3 class='box-title'>Biodata Siswa</h3>
            </div>
            <div class='box-body'>

                <div class='panel-body'>
                    <ul id='myTabs' class='nav nav-tabs' role='tablist'>
                        <li role='presentation' class='active'><a href='#siswa' id='siswa-tab' role='tab' data-toggle='tab' aria-controls='siswa' aria-expanded='true'>Data Siswa </a></li>
                        <li role='presentation' class=''><a href='#ortu' role='tab' id='ortu-tab' data-toggle='tab' aria-controls='ortu' aria-expanded='false'>Data Orang Tua / Wali</a></li>
                        <li role='presentation' class=''><a href='#berkas' role='tab' id='berkas-tab' data-toggle='tab' aria-controls='berkas' aria-expanded='false'>Upload Berkas</a></li>
                    </ul><br>

                    <div id='myTabContent' class='tab-content'>
                        <div role='tabpanel' class='tab-pane fade active in' id='siswa' aria-labelledby='siswa-tab'>
                            <form action='action_simpan_biodata.php' method='POST' enctype='multipart/form-data' class='form-horizontal'>
                                <div class='col-md-6'>
                                    <table class='table table-condensed table-bordered'>
                                        <tbody>
                                            <tr>
                                                <th width='130px' scope='row'>NIPD</th>
                                                <td>
                                                    <input type='hidden' class='form-control' readonly name='id_aktivasi' value="<?php echo $_SESSION['id_user']; ?>">
                                                    <input type='text' class='form-control' required name='nipd'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>NISN</th>
                                                <td><input type='text' class='form-control' required name='nisn'></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Password</th>
                                            <td><input type='text' class='form-control' required name='ac'></td>
                                        </tr> -->
                                            <tr>
                                                <th scope='row'>Nama Siswa</th>
                                                <td><input type='text' class='form-control' required name='nama_siswa'></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Kelas</th>
                                            <td><select class='form-control' required name='ae'>
                                                    <option value='0' selected>- Pilih Kelas -</option>";
                                                    <?php
                                                    $kelas = mysqli_query($connect, "SELECT * FROM kelas");
                                                    while ($a = mysqli_fetch_array($kelas)) {
                                                        echo "<option value='$a[kode_kelas]'>$a[nama_kelas]</option>";
                                                    }
                                                    ?>
                                                    echo "
                                                </select></td>
                                        </tr> -->
                                            <!-- <tr>
                                            <th scope='row'>Angkatan</th>
                                            <td><input type='text' class='form-control' required name='af'></td>
                                        </tr> -->
                                            <!-- <tr>
                                            <th scope='row'>Jurusan</th>
                                            <td><select class='form-control' required name='ag'>
                                                    <option value='0' selected>- Pilih Jurusan -</option>";
                                                    <?php
                                                    $jurusan = mysqli_query($connect, "SELECT * FROM jurusan");
                                                    while ($a = mysqli_fetch_array($jurusan)) {
                                                        echo "<option value='$a[kode_jurusan]'>$a[nama_jurusan]</option>";
                                                    }
                                                    ?>
                                                    echo "
                                                </select></td>
                                        </tr> -->
                                            <tr>
                                                <th scope='row'>Alamat Siswa</th>
                                                <td><input type='text' class='form-control' required name='alamat_siswa' </td> </tr> <tr>
                                                <th scope='row'>RT/RW</th>
                                                <td><input type='text' class='form-control' placeholder="00/00" required name='rt_rw'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Dusun</th>
                                                <td><input type='text' class='form-control' required name='dusun'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kelurahan</th>
                                                <td><input type='text' class='form-control' required name='Kelurahan'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kecamatan</th>
                                                <td><input type='text' class='form-control' required name='Kecamatan'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kode Pos</th>
                                                <td><input type='text' class='form-control' required name='Pos'></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Status Awal</th>
                                            <td><input type='text' class='form-control' required name='an'></td>
                                        </tr> -->

                                            <tr>
                                                <th width='130px' scope='row'>NIK</th>
                                                <td><input type='text' class='form-control' required name='NIK'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Tempat Lahir</th>
                                                <td><input type='text' class='form-control' required name='Lahir_siswa'></td>
                                            </tr>
                                            <!-- <tr>
                                                <th scope='row'>Foto</th>
                                                <td>
                                                    <div style='position:relative;''>
                                                    <a class=' btn btn-primary' href='javascript:;'>
                                                        <input type='file' class='files' required name='ao' onchange='$("#upload-file-info").html($(this).val());'>
                                                        </a> <span style='width:155px' class='label label-info' id='upload-file-info'></span>
                                                    </div>
                                                </td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                                <div class='col-md-6'>
                                    <table class='table table-condensed table-bordered'>
                                        <tbody>

                                            <tr>
                                                <th scope='row'>Tanggal Lahir</th>
                                                <td>
                                                    <input type='date' class='form-control' required name='tgl_Lahir_siswa'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Jenis Kelamin</th>
                                                <td><select class='form-control' required name='Jenis_Kelamin'>
                                                        <option value='0' selected>- Pilih Jenis Kelamin -</option>";
                                                        <?php
                                                        $jk = mysqli_query($connect, "SELECT * FROM jenis_kelamin");
                                                        while ($a = mysqli_fetch_array($jk)) {
                                                        ?>
                                                            <option value='<?php echo $a['id_jenis_kelamin']; ?>'><?php echo $a['jenis_kelamin']; ?></option>";
                                                        <?php  }
                                                        ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Agama</th>
                                                <td><select class='form-control' required name='Agama'>
                                                        <option value='0' selected>- Pilih Agama -</option>";
                                                        <?php
                                                        $agama = mysqli_query($connect, "SELECT * FROM agama");
                                                        while ($a = mysqli_fetch_array($agama)) { ?>
                                                            <option value="<?php echo $a['id_agama']; ?>"><?php echo $a['nama_agama']; ?></option>";
                                                        <?php  }
                                                        ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Keb. Khusus</th>
                                                <td><input type='text' class='form-control' required name='Khusus_siswa'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Jenis Tinggal</th>
                                                <td><input type='text' class='form-control' required name='Jenis_Tinggal'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Transportasi</th>
                                                <td><input type='text' class='form-control' required name='Transportasi'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No Telpon</th>
                                                <td><input type='text' class='form-control' required name='Telpon_siswa'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No Handpone</th>
                                                <td><input type='text' class='form-control' required name='Handpone_siswa'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Alamat Email</th>
                                                <td><input type='text' class='form-control' required name='Alamat_Email'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>SKHUN</th>
                                                <td><input type='text' class='form-control' required name='SKHUN'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Penerima KPS</th>
                                                <td><input type='text' class='form-control' required name='Penerima_KPS'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No KPS</th>
                                                <td><input type='text' class='form-control' required name='No_KPS'></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Status Siswa</th>
                                            <td><input type='radio' required name='bo' value='Aktif' checked> Aktif
                                                <input type='radio' required name='bo' value='Tidak Aktif'> Tidak Aktif </td>
                                        </tr> -->
                                        </tbody>
                                    </table>
                                </div>

                        </div>

                        <div role='tabpanel' class='tab-pane fade' id='ortu' aria-labelledby='ortu-tab'>
                            <div class='col-md-12'>
                                <table class='table table-condensed table-bordered'>
                                    <tbody>
                                        <tr bgcolor=#e3e3e3>
                                            <th width='130px' scope='row'>Nama Ayah</th>
                                            <td><input type='text' class='form-control' required name='Nama_Ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Tahun Lahir</th>
                                            <td><input type='text' class='form-control' required name='Tahun_Lahir_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pendidikan</th>
                                            <td><input type='text' class='form-control' required name='Pendidikan_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pekerjaan</th>
                                            <td><input type='text' class='form-control' required name='Pekerjaan_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Penghasilan</th>
                                            <td><input type='text' class='form-control' required name='Penghasilan_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Kebutuhan Khusus</th>
                                            <td><input type='text' class='form-control' required name='Kebutuhan_Khusus_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>No Telpon</th>
                                            <td><input type='text' class='form-control' required name='No_Telpon_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row' coslpan='2'><br></th>
                                        </tr>
                                        <tr bgcolor=#e3e3e3>
                                            <th scope='row'>Nama Ibu</th>
                                            <td><input type='text' class='form-control' required name='Nama_Ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Tahun Lahir</th>
                                            <td><input type='text' class='form-control' required name='Tahun_Lahir_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pendidikan</th>
                                            <td><input type='text' class='form-control' required name='Pendidikan_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pekerjaan</th>
                                            <td><input type='text' class='form-control' required name='Pekerjaan_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Penghasilan</th>
                                            <td><input type='text' class='form-control' required name='Penghasilan_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Kebutuhan Khusus</th>
                                            <td><input type='text' class='form-control' required name='Kebutuhan_Khusus_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>No Telpon</th>
                                            <td><input type='text' class='form-control' required name='No_Telpon_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row' coslpan='2'><br></th>
                                        </tr>
                                        <tr bgcolor=#e3e3e3>
                                            <th scope='row'>Nama Wali</th>
                                            <td><input type='text' class='form-control' required name='Nama_Wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Tahun Lahir</th>
                                            <td><input type='text' class='form-control' required name='Tahun_Lahir_wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pendidikan</th>
                                            <td><input type='text' class='form-control' required name='Pendidikan_wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pekerjaan</th>
                                            <td><input type='text' class='form-control' required name='Pekerjaan_wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Penghasilan</th>
                                            <td><input type='text' class='form-control' required name='Penghasilan_wali'></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role='tabpanel' class='tab-pane fade' id='berkas' aria-labelledby='berkas-tab'>
                            <div class='col-md-12'>

                                <table class='table table-condensed table-bordered'>
                                    <tbody>
                                        <tr>
                                            <th scope='row'>SKHU</th>
                                            <td>
                                                <div class="form-group">
                                                    <input type="file" name="skhu" required>
                                                    <p class="help-block">Bentuk file. pdf.</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Kartu Keluarga</th>
                                            <td>
                                                <div class="form-group">
                                                    <input type="file" name="kartu_keluarga" required>
                                                    <p class="help-block">Bentuk file. pdf.</p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Upload Rapor</th>
                                            <td>
                                                <div class="form-group">
                                                    <input type="file" name="raport">
                                                    <p class="help-block">Bentuk file. pdf.</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style='clear:both'></div>
                            <div class='box-footer'>
                                <button type='submit' name='tambah' class='btn btn-info'>Simpan</button>
                                <a href='index.php?view=siswa'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php } else { ?>

<?php
}
$tampilkan = mysqli_query($connect, "SELECT * FROM siswa_baru where id_aktivasi ='$_SESSION[id_user]'");
foreach ($tampilkan as $data) {

?>
    <div class='col-md-12'>
        <div class='box box-info'>
            <div class='box-header with-border'>
                <h3 class='box-title'>Biodata Siswa</h3>
            </div>
            <div class='box-body'>

                <div class='panel-body'>
                    <ul id='myTabs' class='nav nav-tabs' role='tablist'>
                        <li role='presentation' class='active'><a href='#siswa' id='siswa-tab' role='tab' data-toggle='tab' aria-controls='siswa' aria-expanded='true'>Data Siswa </a></li>
                        <li role='presentation' class=''><a href='#ortu' role='tab' id='ortu-tab' data-toggle='tab' aria-controls='ortu' aria-expanded='false'>Data Orang Tua / Wali</a></li>
                        <li role='presentation' class=''><a href='#berkas' role='tab' id='berkas-tab' data-toggle='tab' aria-controls='berkas' aria-expanded='false'>Upload Berkas</a></li>

                    </ul><br>

                    <div id='myTabContent' class='tab-content'>
                        <div role='tabpanel' class='tab-pane fade active in' id='siswa' aria-labelledby='siswa-tab'>
                            <form action='action_simpan_biodata.php' method='POST' enctype='multipart/form-data' class='form-horizontal'>
                                <div class='col-md-6'>
                                    <table class='table table-condensed table-bordered'>
                                        <tbody>
                                            <tr>
                                                <th width='130px' scope='row'>NIPD</th>
                                                <td>
                                                    <input type='hidden' class='form-control' readonly name='id_aktivasi' value="<?php echo $_SESSION['id_user']; ?>">
                                                    <input type='text' class='form-control' value="<?php echo @$data['nipd']; ?>" required name='nipd'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>NISN</th>
                                                <td><input type='text' class='form-control' required name='nisn' value="<?php echo @$data['nisn']; ?>"></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Password</th>
                                            <td><input type='text' class='form-control' required name='ac'></td>
                                        </tr> -->
                                            <tr>
                                                <th scope='row'>Nama Siswa</th>
                                                <td><input type='text' class='form-control' required name='nama_siswa' value="<?php echo @$data['nama_baru']; ?>"></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Kelas</th>
                                            <td><select class='form-control' required name='ae'>
                                                    <option value='0' selected>- Pilih Kelas -</option>";
                                                    <?php
                                                    $kelas = mysqli_query($connect, "SELECT * FROM kelas");
                                                    while ($a = mysqli_fetch_array($kelas)) {
                                                        echo "<option value='$a[kode_kelas]'>$a[nama_kelas]</option>";
                                                    }
                                                    ?>
                                                    echo "
                                                </select></td>
                                        </tr> -->
                                            <!-- <tr>
                                            <th scope='row'>Angkatan</th>
                                            <td><input type='text' class='form-control' required name='af'></td>
                                        </tr> -->
                                            <!-- <tr>
                                            <th scope='row'>Jurusan</th>
                                            <td><select class='form-control' required name='ag'>
                                                    <option value='0' selected>- Pilih Jurusan -</option>";
                                                    <?php
                                                    $jurusan = mysqli_query($connect, "SELECT * FROM jurusan");
                                                    while ($a = mysqli_fetch_array($jurusan)) {
                                                        echo "<option value='$a[kode_jurusan]'>$a[nama_jurusan]</option>";
                                                    }
                                                    ?>
                                                    echo "
                                                </select></td>
                                        </tr> -->
                                            <tr>
                                                <th scope='row'>Alamat Siswa</th>
                                                <td><input type='text' class='form-control' required name='alamat_siswa' value="<?php echo @$data['alamat']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>RT/RW</th>
                                                <td><input type='text' class='form-control' value="<?php echo @$data['rt_rw']; ?>" placeholder="00/00" required name='rt_rw'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Dusun</th>
                                                <td><input type='text' class='form-control' required name='dusun' value="<?php echo @$data['dusun']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kelurahan</th>
                                                <td><input type='text' class='form-control' required name='Kelurahan' value="<?php echo @$data['kelurahan']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kecamatan</th>
                                                <td><input type='text' class='form-control' required name='Kecamatan' value="<?php echo @$data['kecamatan']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kode Pos</th>
                                                <td><input type='text' class='form-control' required name='Pos' value="<?php echo @$data['kode_pos']; ?>"></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Status Awal</th>
                                            <td><input type='text' class='form-control' required name='an'></td>
                                        </tr> -->

                                            <tr>
                                                <th width='130px' scope='row'>NIK</th>
                                                <td><input type='text' class='form-control' required name='NIK' value="<?php echo @$data['nik']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Tempat Lahir</th>
                                                <td><input type='text' class='form-control' required value="<?php echo @$data['tempat_lahir']; ?>" name='Lahir_siswa'></td>
                                            </tr>
                                            <!-- <tr>
                                                <th scope='row'>Foto</th>
                                                <td>
                                                    <div style='position:relative;''>
                                                    <a class=' btn btn-primary' href='javascript:;'>
                                                        <input type='file' class='files' required name='ao' onchange='$("#upload-file-info").html($(this).val());'>
                                                        </a> <span style='width:155px' class='label label-info' id='upload-file-info'></span>
                                                    </div>
                                                </td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                                <div class='col-md-6'>
                                    <table class='table table-condensed table-bordered'>
                                        <tbody>

                                            <tr>
                                                <th scope='row'>Tanggal Lahir</th>
                                                <td>

                                                    <input type='text' class='form-control' value="<?php echo date('m-d-Y', strtotime(@$data['tanggal_lahir'])); ?>" required name='tgl_Lahir_siswa'>

                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Jenis Kelamin</th>
                                                <td><select class='form-control' required name='Jenis_Kelamin'>
                                                        <option value='0' selected>- Pilih Jenis Kelamin -</option>";
                                                        <?php
                                                        $jk = mysqli_query($connect, "SELECT * FROM jenis_kelamin");
                                                        while ($a = mysqli_fetch_array($jk)) {
                                                        ?>
                                                            <option <?php if (@$data['id_jenis_kelamin'] == $a['id_jenis_kelamin']) {
                                                                        echo 'selected';
                                                                    } else {
                                                                        echo '';
                                                                    } ?> value='<?php echo $a['id_jenis_kelamin']; ?>'><?php echo $a['jenis_kelamin']; ?></option>";
                                                        <?php  }
                                                        ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Agama</th>
                                                <td><select class='form-control' required name='Agama'>
                                                        <option value='0' selected>- Pilih Agama -</option>";
                                                        <?php
                                                        $agama = mysqli_query($connect, "SELECT * FROM agama");
                                                        while ($a = mysqli_fetch_array($agama)) { ?>
                                                            <option <?php if ($data['id_agama'] == $a['id_agama']) {
                                                                        echo 'selected';
                                                                    } else {
                                                                        echo '';
                                                                    }; ?> value="<?php echo $a['id_agama']; ?>"><?php echo $a['nama_agama']; ?></option>";
                                                        <?php  }
                                                        ?>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Keb. Khusus</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['kebutuhan_khusus']; ?>" required name='Khusus_siswa'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Jenis Tinggal</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['jenis_tinggal']; ?>" required name='Jenis_Tinggal'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Transportasi</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['alat_transportasi']; ?>" required name='Transportasi'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No Telpon</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['telepon']; ?>" required name='Telpon_siswa'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No Handpone</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['hp']; ?>" required name='Handpone_siswa'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Alamat Email</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['email']; ?>" required name='Alamat_Email'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>SKHUN</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['skhun']; ?>" required name='SKHUN'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Penerima KPS</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['penerima_kps']; ?>" required name='Penerima_KPS'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No KPS</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['no_kps']; ?>" required name='No_KPS'></td>
                                            </tr>
                                            <!-- <tr>
                                            <th scope='row'>Status Siswa</th>
                                            <td><input type='radio' required name='bo' value='Aktif' checked> Aktif
                                                <input type='radio' required name='bo' value='Tidak Aktif'> Tidak Aktif </td>
                                        </tr> -->
                                        </tbody>
                                    </table>
                                </div>

                        </div>

                        <div role='tabpanel' class='tab-pane fade' id='ortu' aria-labelledby='ortu-tab'>
                            <div class='col-md-12'>
                                <table class='table table-condensed table-bordered'>
                                    <tbody>
                                        <tr bgcolor=#e3e3e3>
                                            <th width='130px' scope='row'>Nama Ayah</th>
                                            <td><input type='text' class='form-control' required name='Nama_Ayah' value="<?php echo $data['nama_ayah']; ?>"></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Tahun Lahir</th>
                                            <td><input type='text' class='form-control' required value="<?php echo $data['tahun_lahir_ayah']; ?>" name='Tahun_Lahir_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pendidikan</th>
                                            <td><input type='text' class='form-control' required value="<?php echo $data['pendidikan_ayah']; ?>" name='Pendidikan_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pekerjaan</th>
                                            <td><input type='text' class='form-control' required value="<?php echo $data['pekerjaan_ayah']; ?>" name='Pekerjaan_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Penghasilan</th>
                                            <td><input type='text' class='form-control' required value="<?php echo $data['penghasilan_ayah']; ?>" name='Penghasilan_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Kebutuhan Khusus</th>
                                            <td><input type='text' class='form-control' required value="<?php echo $data['kebutuhan_khusus_ayah']; ?>" name='Kebutuhan_Khusus_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>No Telpon</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['no_telpon_ayah']; ?>" required name='No_Telpon_ayah'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row' coslpan='2'><br></th>
                                        </tr>
                                        <tr bgcolor=#e3e3e3>
                                            <th scope='row'>Nama Ibu</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['nama_ibu']; ?>" required name='Nama_Ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Tahun Lahir</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['tahun_lahir_ibu']; ?>" required name='Tahun_Lahir_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pendidikan</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['pendidikan_ibu']; ?>" required name='Pendidikan_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pekerjaan</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['pekerjaan_ibu']; ?>" required name='Pekerjaan_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Penghasilan</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['penghasilan_ibu']; ?>" required name='Penghasilan_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Kebutuhan Khusus</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['kebutuhan_khusus_ibu']; ?>" required name='Kebutuhan_Khusus_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>No Telpon</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['no_telpon_ibu']; ?>" required name='No_Telpon_ibu'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row' coslpan='2'><br></th>
                                        </tr>
                                        <tr bgcolor=#e3e3e3>
                                            <th scope='row'>Nama Wali</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['nama_wali']; ?>" required name='Nama_Wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Tahun Lahir</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['tahun_lahir_wali']; ?>" required name='Tahun_Lahir_wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pendidikan</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['pendidikan_wali']; ?>" required name='Pendidikan_wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Pekerjaan</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['pekerjaan_wali']; ?>" required name='Pekerjaan_wali'></td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Penghasilan</th>
                                            <td><input type='text' class='form-control' value="<?php echo $data['penghasilan_wali']; ?>" required name='Penghasilan_wali'></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div role='tabpanel' class='tab-pane fade' id='berkas' aria-labelledby='berkas-tab'>
                            <div class='col-md-12'>

                                <table class='table table-condensed table-bordered'>
                                    <tbody>
                                        <tr>
                                            <th style="width: 200px;" scope='row'>SKHU</th>
                                            <td style="width: 350px;">
                                                <div class="form-group">
                                                    <input type="file" name="skhu">
                                                    <br>
                                                    <input type="hidden" name="skhu2" readonly value="<?php echo $data['file_skhu']; ?>">
                                                    <p class="help-block">Bentuk file. pdf.</p>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="file_documen/<?php echo $data['file_skhu'];?>" class="btn bg-olive btn-flat margin">Download</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Kartu Keluarga</th>
                                            <td>
                                                <div class="form-group">
                                                    <input type="file" name="kartu_keluarga">
                                                    <br>
                                                    <input type="hidden" name="kartu_keluarga2" readonly value="<?php echo $data['file_kk']; ?>">

                                                    <p class="help-block">Bentuk file. pdf.</p>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="file_documen/<?php echo $data['file_kk'];?>" class="btn bg-olive btn-flat margin">Download</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope='row'>Upload Rapor</th>
                                            <td>
                                                <div class="form-group">
                                                    <input type="file" name="raport">
                                                    <br>
                                                    <input type="hidden" name="raport2" readonly value="<?php echo $data['file_rapor']; ?>">
                                                    <p class="help-block">Bentuk file. pdf.</p>
                                                </div>
                                            </td>
                                            <td>
                                                <a target="_blank" href="file_documen/<?php echo $data['file_rapor'];?>" class="btn bg-olive btn-flat margin">Download</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class='box-footer'>
                                <button type='submit' name='tambah' class='btn btn-info'>Update</button>
                                <a href='index.php?view=siswa'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php } ?>