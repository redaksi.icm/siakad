<?php
session_start();
// error_reporting(0);
include "config/koneksi.php";
include "config/fungsi_indotgl.php";
?>

<head>
  <title>Data Penerimaan Siswa Baru</title>
  <link rel="stylesheet" href="bootstrap/css/printer.css">
</head>

<body onload="window.print()">

  <?php
  $tampilkan = mysqli_query($connect, "SELECT * FROM siswa_baru 
  Left join agama on siswa_baru.id_agama=agama.id_agama 
  where id_siswa_baru ='$_GET[id]'");


  foreach ($tampilkan as $data) {
  ?>
    <table width=100%>
      <tr>
        <td width='90px'><img style='width:80px' src='print_raport/logo.png'>
        </td>
        <td><b>SMAN 1 <br> PALEMBAYAN </b><br> Jl.AR Hakim No.57 Telp. (0751) xxxxx Padang</td>
      </tr>
    </table>
    <hr>
    <h3>
      <center>FORMULIR PENDAFTARAN <br>DATA SISWA</center>
    </h3>
    <table width='100%'>
      <tr>
        <td width='160px'>Nama Siswa</td>
        <td>:</td>
        <td><?php echo @$data['nama_baru']; ?></td>
      </tr>
      <tr>
        <td>Tempat / Tgl Lahir</td>
        <td>:</td>
        <td><?php echo @$data['tempat_lahir']; ?>, <?php echo date('m-d-Y', strtotime(@$data['tanggal_lahir'])); ?></td>
      </tr>
      <tr>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php if ($data['id_jenis_kelamin']  == 1) {
              echo 'Laki-laki';
            } else {
              echo 'Perempuan';
            }; ?></td>
      </tr>
      <tr>
        <td>Agama</td>
        <td>:</td>
        <td><?php echo $data['nama_agama'] ?></td>
      </tr>
      <tr>
        <td>Alamat Rumah</td>
        <td>:</td>
        <td><?php echo @$data['alamat']; ?></td>
      </tr>
      <tr>
        <td>Telepon</td>
        <td>:</td>
        <td><?php echo $data['telepon']; ?></td>
      </tr>
    </table>

    <h3>
      <center>DATA ORANG TUA / WALI SISWA</center>
    </h3>
    <table width='100%' id='tablemodul1'>
      <tr>
        <th>
          <center>Keterangan</center>
        </th>
        <th>
          <center>Data Ayah</center>
        </th>
        <th>
          <center>Data Ibu</center>
        </th>
      </tr>
      <tr>
        <td width='170px'>Nama Lengkap</td>
        <td><?php echo $data['nama_ayah']; ?></td>
        <td><?php echo $data['nama_ibu']; ?></td>
      </tr>
      <tr>
        <td>Tahun Lahir</td>
        <td><?php echo $data['tahun_lahir_ayah']; ?></td>
        <td><?php echo $data['tahun_lahir_ibu']; ?></td>
      </tr>
      <tr>
        <td>Pekerjaan</td>
        <td><?php echo $data['pekerjaan_ayah']; ?></td>
        <td><?php echo $data['pekerjaan_ibu']; ?></td>
      </tr>
      <tr>
        <td>Telepon / Handphone</td>
        <td><?php echo $data['no_telpon_ayah']; ?></td>
        <td><?php echo $data['no_telpon_ibu']; ?></td>
      </tr>
      <tr>
        <td>Telepon Kantor</td>
        <td>$s[telpon_kantor_ayah]</td>
        <td>$s[telpon_kantor_ibu]</td>
      </tr>
    </table>

  <?php } ?>


  Saya yang bertanda tangan dibawah ini , mendaftarkan anak saya sebagai siswa/i dan bersedia mengikuti persyaratan dan peraturan yang berlaku.
  <br>
  <table border=0 width=100%>
    <tr>
      <td width="400" align="left"><br>Padang, <?php echo tgl_raport(date("Y-m-d")); ?></td>
    </tr>
    <tr>
      <td align="left"><br /><br />
        ................................... </td>

    </tr>
  </table>
</body>