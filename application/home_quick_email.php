              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-envelope"></i>
                  <h3 class="box-title">Kirimkan E-mail</h3>
                  <?php
                  if (@$_GET['status'] == 'berhasil') {
                    echo "<center>Email sent successfully !!</center>";
                  } else {
                    echo "<center>Email sent Gagal !!</center>";
                  }
                  ?>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <form action="" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" placeholder="Subject">
                    </div>
                    <div>
                      <textarea name='message' placeholder="Message" style="width: 100%; height: 232px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                    <?php

                    use PHPMailer\PHPMailer\PHPMailer;
                    use PHPMailer\PHPMailer\Exception;

                    require_once "library/PHPMailer.php";
                    require_once "library/Exception.php";
                    require_once "library/OAuth.php";
                    require_once "library/POP3.php";
                    require_once "library/SMTP.php";
                    $mail = new PHPMailer;

                    if (isset($_POST['kirim'])) {
                      $name = 'sisteminformasi-sekolah.co';
                      $email = $_POST['emailto'];
                      $subject = $_POST['subject'];
                      $message = $_POST['message'];
                      //Enable SMTP debugging. 
                      $mail->SMTPDebug = 3;
                      //Set PHPMailer to use SMTP.
                      $mail->isSMTP();
                      //Set SMTP host name                          
                      $mail->Host = "tls://smtp.gmail.com"; //host mail server
                      //Set this to true if SMTP host requires authentication to send email
                      $mail->SMTPAuth = true;
                      //Provide username and password     
                      $mail->Username = "smansapaa@gmail.com";   //nama-email smtp          
                      $mail->Password = "smansapa1983";           //password email smtp
                      //If SMTP requires TLS encryption then set it
                      $mail->SMTPSecure = "tls";
                      //Set TCP port to connect to 
                      $mail->Port = 587;

                      $mail->From = "smansapaa@gmail.com"; //email pengirim
                      $mail->FromName = "sisteminformasi-sekolah.co"; //nama pengirim

                      $mail->addAddress($email); //email penerima

                      $mail->isHTML(true);

                      $mail->Subject = $subject; //subject
                      $mail->Body    = $message; //isi email
                      $mail->AltBody = "PHP mailer"; //body email (optional)

                      if (!$mail->send()) {
                        // echo "<center>Email sent Gagal !!</center>";
                        echo "<script>document.location='index.php?view=home&status=gagal';</script>";
                      } else {
                        // echo "<center>Email sent successfully !!</center>";
                        echo "<script>document.location='index.php?view=home&status=berhasil';</script>";
                      }
                    }
                    ?>



                </div>
                <div class="box-footer clearfix">
                  <button type='submit' name='kirim' class="pull-right btn btn-default" id="sendEmail">Send <i class="fa fa-arrow-circle-right"></i></button>
                </div>
                </form>
              </div>