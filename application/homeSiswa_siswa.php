<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">PENGUMUMAN</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body text-muted well well-sm no-shadow">
            <?php
            $detail = mysqli_query($connect, "SELECT * FROM tbl_pengumuman order by id_pengumuan desc");

            foreach ($detail as $data) {
            ?>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-item bg-navy disabled color-palette" style="padding: 5px;">
                            <h3 class="timeline-header"><a href="#"><i style="color: white;"><?php echo $data['judul']; ?></i></h3>
                            <div class="timeline-body text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                <img src="foto_pengumuman/<?php echo $data['file_foto']; ?>" alt="" style="height: 330px;width: 100%;">
                            </div>
                            <!-- </p> -->
                            <div class="timeline-footer">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
                <br>
            <?php } ?>
        </div>
    </div>
</section>