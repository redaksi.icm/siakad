<?php
if (@$_GET['act'] == '') {
    if ($_GET['view'] == 'baru') {
        cek_session_admin();
?>

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Semua Data Siswa PSB </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id='example1' class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Induk</th>
                                <th>Kode Pendaftaran</th>
                                <th>Nama Siswa</th>
                                <th>Jenis Kelamin</th>
                                <th>No Telpon</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM siswa_baru 
                            left join psb_aktivasi on psb_aktivasi.id_aktivasi = siswa_baru.id_aktivasi");
                            foreach ($tampilkan as $data) { ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['nipd']; ?></td>
                                    <td><?php echo $data['kode_pendaftaran']; ?></td>
                                    <td><?php echo $data['nama_baru']; ?></td>
                                    <td><?php if ($data['id_jenis_kelamin'] == 1) {
                                            echo 'Laki-laki';
                                        } else {
                                            echo 'Perempuan';
                                        } ?></td>
                                    <td><?php echo $data['hp']; ?></td>

                                    <td>
                                        <a class='btn btn-info btn-xs' title='Lihat Detail' href='?view=baru&act=detail&id=<?php echo $data["id_siswa_baru"]; ?>'><span class='glyphicon glyphicon-search'></span></a>
                                        <a class='btn btn-success btn-xs' target='_BLANK' title='Bukti Pendaftaran' href='print-psb1.php?id=<?php echo $data["id_siswa_baru"]; ?>'><span class='glyphicon glyphicon-print'></span> </a>
                                    </td>
                                </tr>
                            <?php }  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } elseif ($_GET['act'] == 'detail') { ?>
    <?php

    $tampilkan = mysqli_query($connect, "SELECT * FROM siswa_baru where id_siswa_baru ='$_GET[id]'");
    foreach ($tampilkan as $data) {
    ?>
        <div class='col-md-12'>
            <div class='box box-info'>
                <div class='box-header with-border'>
                    <h3 class='box-title'>Biodata Siswa</h3>
                </div>
                <div class='box-body'>

                    <div class='panel-body'>
                        <ul id='myTabs' class='nav nav-tabs' role='tablist'>
                            <li role='presentation' class='active'><a href='#siswa' id='siswa-tab' role='tab' data-toggle='tab' aria-controls='siswa' aria-expanded='true'>Data Siswa </a></li>
                            <li role='presentation' class=''><a href='#ortu' role='tab' id='ortu-tab' data-toggle='tab' aria-controls='ortu' aria-expanded='false'>Data Orang Tua / Wali</a></li>
                            <li role='presentation' class=''><a href='#berkas' role='tab' id='berkas-tab' data-toggle='tab' aria-controls='berkas' aria-expanded='false'>Upload Berkas</a></li>

                        </ul><br>

                        <div id='myTabContent' class='tab-content'>
                            <div role='tabpanel' class='tab-pane fade active in' id='siswa' aria-labelledby='siswa-tab'>
                                <form action='action_simpan_biodata.php' method='POST' enctype='multipart/form-data' class='form-horizontal'>
                                    <div class='col-md-6'>
                                        <table class='table table-condensed table-bordered'>
                                            <tbody>
                                                <tr>
                                                    <th width='130px' scope='row'>NIPD</th>
                                                    <td>
                                                        <input type='hidden' class='form-control' readonly name='id_aktivasi' value="<?php echo $_SESSION['id_user']; ?>">
                                                        <input type='text' class='form-control' value="<?php echo @$data['nipd']; ?>" required readonly name='nipd'>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>NISN</th>
                                                    <td><input type='text' class='form-control' required readonly name='nisn' value="<?php echo @$data['nisn']; ?>"></td>
                                                </tr>
                                                <!-- <tr>
                                            <th scope='row'>Password</th>
                                            <td><input type='text' class='form-control' required readonly name='ac'></td>
                                        </tr> -->
                                                <tr>
                                                    <th scope='row'>Nama Siswa</th>
                                                    <td><input type='text' class='form-control' required readonly name='nama_siswa' value="<?php echo @$data['nama_baru']; ?>"></td>
                                                </tr>
                                                <!-- <tr>
                                            <th scope='row'>Kelas</th>
                                            <td><select class='form-control' required readonly name='ae'>
                                                    <option value='0' selected>- Pilih Kelas -</option>";
                                                    <?php
                                                    $kelas = mysqli_query($connect, "SELECT * FROM kelas");
                                                    while ($a = mysqli_fetch_array($kelas)) {
                                                        echo "<option value='$a[kode_kelas]'>$a[nama_kelas]</option>";
                                                    }
                                                    ?>
                                                    echo "
                                                </select></td>
                                        </tr> -->
                                                <!-- <tr>
                                            <th scope='row'>Angkatan</th>
                                            <td><input type='text' class='form-control' required readonly name='af'></td>
                                        </tr> -->
                                                <!-- <tr>
                                            <th scope='row'>Jurusan</th>
                                            <td><select class='form-control' required readonly name='ag'>
                                                    <option value='0' selected>- Pilih Jurusan -</option>";
                                                    <?php
                                                    $jurusan = mysqli_query($connect, "SELECT * FROM jurusan");
                                                    while ($a = mysqli_fetch_array($jurusan)) {
                                                        echo "<option value='$a[kode_jurusan]'>$a[nama_jurusan]</option>";
                                                    }
                                                    ?>
                                                    echo "
                                                </select></td>
                                        </tr> -->
                                                <tr>
                                                    <th scope='row'>Alamat Siswa</th>
                                                    <td><input type='text' class='form-control' required readonly name='alamat_siswa' value="<?php echo @$data['alamat']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>RT/RW</th>
                                                    <td><input type='text' class='form-control' value="<?php echo @$data['rt_rw']; ?>" placeholder="00/00" required readonly name='rt_rw'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Dusun</th>
                                                    <td><input type='text' class='form-control' required readonly name='dusun' value="<?php echo @$data['dusun']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Kelurahan</th>
                                                    <td><input type='text' class='form-control' required readonly name='Kelurahan' value="<?php echo @$data['kelurahan']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Kecamatan</th>
                                                    <td><input type='text' class='form-control' required readonly name='Kecamatan' value="<?php echo @$data['kecamatan']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Kode Pos</th>
                                                    <td><input type='text' class='form-control' required readonly name='Pos' value="<?php echo @$data['kode_pos']; ?>"></td>
                                                </tr>
                                                <!-- <tr>
                                            <th scope='row'>Status Awal</th>
                                            <td><input type='text' class='form-control' required readonly name='an'></td>
                                        </tr> -->

                                                <tr>
                                                    <th width='130px' scope='row'>NIK</th>
                                                    <td><input type='text' class='form-control' required readonly name='NIK' value="<?php echo @$data['nik']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Tempat Lahir</th>
                                                    <td><input type='text' class='form-control' required value="<?php echo @$data['tempat_lahir']; ?>" readonly name='Lahir_siswa'></td>
                                                </tr>
                                                <!-- <tr>
                                                <th scope='row'>Foto</th>
                                                <td>
                                                    <div style='position:relative;''>
                                                    <a class=' btn btn-primary' href='javascript:;'>
                                                        <input type='file' class='files' required readonly name='ao' onchange='$("#upload-file-info").html($(this).val());'>
                                                        </a> <span style='width:155px' class='label label-info' id='upload-file-info'></span>
                                                    </div>
                                                </td>
                                            </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class='col-md-6'>
                                        <table class='table table-condensed table-bordered'>
                                            <tbody>

                                                <tr>
                                                    <th scope='row'>Tanggal Lahir</th>
                                                    <td>

                                                        <input type='text' class='form-control' value="<?php echo date('m-d-Y', strtotime(@$data['tanggal_lahir'])); ?>" required readonly name='tgl_Lahir_siswa'>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Jenis Kelamin</th>
                                                    <td><select class='form-control' required readonly name='Jenis_Kelamin'>
                                                            <option value='0' selected>- Pilih Jenis Kelamin -</option>";
                                                            <?php
                                                            $jk = mysqli_query($connect, "SELECT * FROM jenis_kelamin");
                                                            while ($a = mysqli_fetch_array($jk)) {
                                                            ?>
                                                                <option <?php if (@$data['id_jenis_kelamin'] == $a['id_jenis_kelamin']) {
                                                                            echo 'selected';
                                                                        } else {
                                                                            echo '';
                                                                        } ?> value='<?php echo $a['id_jenis_kelamin']; ?>'><?php echo $a['jenis_kelamin']; ?></option>";
                                                            <?php  }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Agama</th>
                                                    <td><select class='form-control' required readonly name='Agama'>
                                                            <option value='0' selected>- Pilih Agama -</option>";
                                                            <?php
                                                            $agama = mysqli_query($connect, "SELECT * FROM agama");
                                                            while ($a = mysqli_fetch_array($agama)) { ?>
                                                                <option <?php if ($data['id_agama'] == $a['id_agama']) {
                                                                            echo 'selected';
                                                                        } else {
                                                                            echo '';
                                                                        }; ?> value="<?php echo $a['id_agama']; ?>"><?php echo $a['nama_agama']; ?></option>";
                                                            <?php  }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Keb. Khusus</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['kebutuhan_khusus']; ?>" required readonly name='Khusus_siswa'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Jenis Tinggal</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['jenis_tinggal']; ?>" required readonly name='Jenis_Tinggal'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Transportasi</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['alat_transportasi']; ?>" required readonly name='Transportasi'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>No Telpon</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['telepon']; ?>" required readonly name='Telpon_siswa'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>No Handpone</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['hp']; ?>" required readonly name='Handpone_siswa'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Alamat Email</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['email']; ?>" required readonly name='Alamat_Email'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>SKHUN</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['skhun']; ?>" required readonly name='SKHUN'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>Penerima KPS</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['penerima_kps']; ?>" required readonly name='Penerima_KPS'></td>
                                                </tr>
                                                <tr>
                                                    <th scope='row'>No KPS</th>
                                                    <td><input type='text' class='form-control' value="<?php echo $data['no_kps']; ?>" required readonly name='No_KPS'></td>
                                                </tr>
                                                <!-- <tr>
                                            <th scope='row'>Status Siswa</th>
                                            <td><input type='radio' required readonly name='bo' value='Aktif' checked> Aktif
                                                <input type='radio' required readonly name='bo' value='Tidak Aktif'> Tidak Aktif </td>
                                        </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                            </div>

                            <div role='tabpanel' class='tab-pane fade' id='ortu' aria-labelledby='ortu-tab'>
                                <div class='col-md-12'>
                                    <table class='table table-condensed table-bordered'>
                                        <tbody>
                                            <tr bgcolor=#e3e3e3>
                                                <th width='130px' scope='row'>Nama Ayah</th>
                                                <td><input type='text' class='form-control' required readonly name='Nama_Ayah' value="<?php echo $data['nama_ayah']; ?>"></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Tahun Lahir</th>
                                                <td><input type='text' class='form-control' required value="<?php echo $data['tahun_lahir_ayah']; ?>" readonly name='Tahun_Lahir_ayah'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Pendidikan</th>
                                                <td><input type='text' class='form-control' required value="<?php echo $data['pendidikan_ayah']; ?>" readonly name='Pendidikan_ayah'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Pekerjaan</th>
                                                <td><input type='text' class='form-control' required value="<?php echo $data['pekerjaan_ayah']; ?>" readonly name='Pekerjaan_ayah'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Penghasilan</th>
                                                <td><input type='text' class='form-control' required value="<?php echo $data['penghasilan_ayah']; ?>" readonly name='Penghasilan_ayah'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kebutuhan Khusus</th>
                                                <td><input type='text' class='form-control' required value="<?php echo $data['kebutuhan_khusus_ayah']; ?>" readonly name='Kebutuhan_Khusus_ayah'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No Telpon</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['no_telpon_ayah']; ?>" required readonly name='No_Telpon_ayah'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row' coslpan='2'><br></th>
                                            </tr>
                                            <tr bgcolor=#e3e3e3>
                                                <th scope='row'>Nama Ibu</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['nama_ibu']; ?>" required readonly name='Nama_Ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Tahun Lahir</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['tahun_lahir_ibu']; ?>" required readonly name='Tahun_Lahir_ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Pendidikan</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['pendidikan_ibu']; ?>" required readonly name='Pendidikan_ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Pekerjaan</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['pekerjaan_ibu']; ?>" required readonly name='Pekerjaan_ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Penghasilan</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['penghasilan_ibu']; ?>" required readonly name='Penghasilan_ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kebutuhan Khusus</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['kebutuhan_khusus_ibu']; ?>" required readonly name='Kebutuhan_Khusus_ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>No Telpon</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['no_telpon_ibu']; ?>" required readonly name='No_Telpon_ibu'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row' coslpan='2'><br></th>
                                            </tr>
                                            <tr bgcolor=#e3e3e3>
                                                <th scope='row'>Nama Wali</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['nama_wali']; ?>" required readonly name='Nama_Wali'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Tahun Lahir</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['tahun_lahir_wali']; ?>" required readonly name='Tahun_Lahir_wali'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Pendidikan</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['pendidikan_wali']; ?>" required readonly name='Pendidikan_wali'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Pekerjaan</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['pekerjaan_wali']; ?>" required readonly name='Pekerjaan_wali'></td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Penghasilan</th>
                                                <td><input type='text' class='form-control' value="<?php echo $data['penghasilan_wali']; ?>" required readonly name='Penghasilan_wali'></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                </form>
                            </div>
                            <div role='tabpanel' class='tab-pane fade' id='berkas' aria-labelledby='berkas-tab'>
                                <div class='col-md-12'>

                                    <table class='table table-condensed table-bordered'>
                                        <tbody>
                                            <tr>
                                                <th style="width: 200px;" scope='row'>SKHU</th>

                                                <td>
                                                    <a target="_blank" href="login_psb/file_documen/<?php echo $data['file_skhu']; ?>" class="btn bg-olive btn-flat margin">Download</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Kartu Keluarga</th>

                                                <td>
                                                    <a target="_blank" href="login_psb/file_documen/<?php echo $data['file_kk']; ?>" class="btn bg-olive btn-flat margin">Download</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope='row'>Upload Rapor</th>

                                                <td>
                                                    <a target="_blank" href="login_psb/file_documen/<?php echo $data['file_rapor']; ?>" class="btn bg-olive btn-flat margin">Download</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>