<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Sejarah SMAN 1 PALEMBAYAN</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <p style="text-align: justify;">
                    Sebelum SMA N 1 Palembayan didirikan, banyak anak nagari yang melanjutkan sekolah mereka dengan pergi bersekolah jauh ke Kecamatan palembayan karena belum adanya SMA di daerah mereka. Banyak diantara mereka yang harus memendam cita-cita mereka untuk melanjutkan pendidikan ke tingkat SMA karena mengingat situasi dan kondisi perekonomian keluarga mereka yang tidak mencukupi untuk bisa bersekolah ke luar daerah tersebut.
                    Untuk menangani ini semua, maka pada tahun 1988 berdirilah suatu yayasan yang bernama YPDR (Yayasan Pendidikan Dokter Risma) yang waktu itu diketuai oleh M.Dt. Rajo nan Panjang dengan kepala sekolah adalah Bapak Amarullah. Jumlah siswanya yang pertama kali sebanyak tujuh orang dengan local yang masih menompang pada SD 04 Pasar Palembayan. Kemudian karena ada suatu masalah, yayasan ini dipindahkan ke SD Impres Padang Datar. YPDR ini ternyata mengalami perkembangan yang memuaskan. Melihat itu semua, maka masyarakat kenagarian mengadakan suatu acara halal bihalal dan menyepakati untuk mengadakan musyawarah untuk menampung anak-anak nagari di Kanagarian IV Koto Palembayan dan sekitarnya. Pada saat itu disadari betul betapa pentingnya sekolah untuk kemajuan nagari.
                    Ide untuk pembangunan SMA tersebut diajukan kepada Kanwil Depdikbud yang ternyata proposal ini mendapat tanggapan yang positif dari pemerintah. Tahap pembangunan awal terdiri dari enam ruangan yang meliputi ruang kepala sekolah, tata usaha, ruang majelis guru, ruang belajar dan WC.
                    Walaupun secara informal gedung ini telah dipakai, namun peresmiannya baru dilakukan tahun 1991 oleh Wakil MPR/ DPR RI yaitu Bapak Syaiful Sultan sekaligus keluarnya SK negeri dari pemerintah dengan mengganti YPDR menjadi SMA N 1 Palembayan. Adapun yang pernah menjabat sebagai kepala SMA N 1 Palembayan adalah:
                </p>
                <p>a. Drs. Awaludin Thalib (1991 s/d 1996)</p>
                <p>b. Drs. Masril (1996 s/d 2001)</p>
                <p>c. Drs. Lismar Mahmud (2001 s/d 2002)</p>
                <p>d. Drs. Hamdi (2002 s/d 2004)</p>
                <p>e. Zulfikar, S.Pd (2004 s/d 2008)</p>
                <p>f. Haswir, S.Pd (2008 s/d 2011)</p>
                <p>g. Drs. Marta Darma (2011 s/d 2015)</p>
                <p>h. Drs. Wannasri (2015 s/d 2019)</p>
                <p>i. Harpizon Astani, S.Pd, M.Si (2019 s/d sekarang)</p>
            </div>
        </div>
    </div>
</div>
<!-- batas -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">VISI-MISI Sejarah SMAN 1 PALEMBAYAN</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <p>
                    <p>VISI :</p>
                    <p>TERWUJUDNYA SUMBER DAYA MANUSIA YANG BERKUALITAS, BERIMAN DAN BERTAQWA</p>


                    <p>MISI :</p>
                    <p>1. MENINGKATKAN SUMBER DAYA MANUSIA YANG BERPRESTASI DAN MAMPU BERSAING DALAM MEMASUKI PERGURUAN TINGGI.</p>
                    <p>2. MENUMBUHKEMBANGKAN ETOS KERJA DAN SEMANGAT KEUNGGULAN SELURUH WARGA SEKOLAH.</p>
                    <p>3. MENERAPKAN MANAJEMEN PARTISIPASIF YANG BERSIFAT KEKELUARGAAN DENGAN MELIBATKAN SELURUH WARGA SEKOLAH.</p>
                    <p>4. MEMBERIKAN RASA AMAN DAN KESENANGAN BAGI WARGA SEKOLAH DAN MASYARAKAT.</p>
                    <p>5. MENUMBUHKAN BUDAYA MUTU DI LINGKUNGAN SEKOLAH.</p>
                    <p>6. MENINGKATKAN KEMAMPUAN PROFESIONALISME TENAGA PENDIDIK DAN TENAGA KEPENDIDIKAN.</p>
                    <p>7. MENINGKATKAN NUANSA KEHIDUPAN ISLAMI DI SEKOLAH.</p>

                </p>
            </div>
        </div>
    </div>
</div>